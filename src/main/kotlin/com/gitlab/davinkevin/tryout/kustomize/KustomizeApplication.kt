package com.gitlab.davinkevin.tryout.kustomize

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router
import java.nio.file.Files
import java.nio.file.Paths

@SpringBootApplication
class KustomizeApplication

fun main(args: Array<String>) {
    runApplication<KustomizeApplication>(*args)
}

@Configuration
@EnableConfigurationProperties(FooProperties::class, ApiProperties::class)
class RouterConfig {

    val log = LoggerFactory.getLogger(RouterConfig::class.java)

    @Bean
    fun router(p: FooProperties, pass: ApiProperties) = router {
        GET("/") {
            log.info("The credential is ${pass.password}")

            val file = Paths.get("/tmp/key/duke.txt")

            if (Files.exists(file)) {
                log.info("The file contains the value: ${String(Files.readAllBytes(file))}")
            } else {
                log.info("File $file not found")
            }

            ok().syncBody(p)
        }
    }
}

@ConfigurationProperties("message")
class FooProperties {
    lateinit var from: String
    lateinit var body: String
}

@ConfigurationProperties("sensitive")
class ApiProperties {
    lateinit var password: String
}
